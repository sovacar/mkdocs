# 2. External Web Authentication (EWA)

## Task

You have been tasked to configure 9800 series wireless controller with a guest SSID that will be using an existing custom web authentication portal over at **https://portal.thewifilab.net/login.html**. The end customer would like to try out their newly deployed Cisco ISE 3.0 that is running on the AWS to authenticate guest users via RADIUS protocol.


<style>
.alert {
  padding: 20px;
  background-color: #fdfd96;
  color: black;
}
</style>

<div class="alert">
  <strong>Note:</strong> This task will be building on top of the LWA configuration from previous chapter. Please make sure the virtual IP address and trustpoint are configured in the global parameter map.
</div>
<br>


## Web Auth Parameter Map
1.Go to **Configuration** > **Security** > **Web Auth** and create a new webauth parameter:

 - **Name** = `EWA`
 - **Type** = `webauth`
    
<img src="ewa/ewa-01.png" width="100%">
    
<hr>

2.Once saved, click again on the newly created `EWA` parameter map and navigate to **Advanced** tab and add the following:

 - **Redirect for log-in** = `https://portal.thewifilab.net/login.html`
 - **IPv4** = `3.125.154.149`


<img src="ewa/ewa-02.png" width="100%">

<hr>



<div class="alert">
  <strong>Note:</strong> When the Portal IPV4 Address is added, the WLC automatically creates 2 access lists:</p>
  <p><code>WA-sec-3.125.154.149</code><br>
 <code>WA-v4-int-3.125.154.149</code>
</code> </p>
<p><b>WA-sec</b> ACL is a classic pre-authentication ACL which permits DHCP, DNS and HTTP/HTTPS traffic towards the portal IP address. Goal of this ACL is to stop clients from accessing anything other than the most necessary resources to complete web authentication. This ACL is removed from the client once it completes the authentication and reaches the RUN state. Custom preauth ACL can also be defined if needed.

<p><b>WA-v4-int</b> is a <b>intercept/redirect</b> ACL that tells the controller which traffic should not be intercepted. Deny = do not intercept this traffic. Generally, there is no need to configure custom redirect ACLs.


</div>
<br>

<img src="ewa/ewa-03.png" width="100%">

<hr>
<img src="ewa/ewa-04.png" width="100%">

<hr>

## Trivia 


The colleague who stole your promotion has complained to your boss and tried to get you off this project. He says that you didn't even remember to enable HTTPS interception on the global webauth parameter and that this proves you don't know what you are doing. What do you say to your boss? Is your colleague actually right? 
	

<button type="button" class="btn btn-warning" id="myBtn">I need the answer!</button>


<style>


  /* The Modal (background) */
.modal {
 display: none; /* Hidden by default */
 position: fixed; /* Stay in place */
 z-index: 1; /* Sit on top */
 left: 0;
 top: 0;
 width: 100%; /* Full width */
 height: 100%; /* Full height */
 overflow: auto; /* Enable scroll if needed */
 background-color: rgb(0,0,0); /* Fallback color */
 background-color: rgba(0, 0, 0, 0.751); /* Black w/ opacity */
}


/* Modal Content/Box */
.modal-content {
 background-color: #fefefe;
 margin-top: 25%;
 margin-bottom: 5%;
 margin-left: 320px;
 margin-right: 20%;
 padding: 20px;
 border: 1px solid #888;
 border-radius: 15px;
}

/* The Close Button */
.close {
 color: #aaa;
 float: right;
 font-size: 28px;
 font-weight: bold;
}

.close:hover,
.close:focus {
 color: black;
 text-decoration: none;
 cursor: pointer;
} 
</style>



<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">

    <span class="close">&times;</span>
    <p>HTTPS interception is evil! It should never be enabled.
    <hr>
    <p>Enabling HTTPS interception consumes a lot of WLC CPU resources and eventually results in a certificate error. When a client tries to reach out to, lets say, https://youtube.com and ends up being presented with the certificate for https://portal.thewifilab.net where the WLC redirect it, the browser will throw a certificate warning. Chances are, the guest user will simply switch to 4G after seeing the scary error message.

  </div>
<br>

</div> 
<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
 modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
 modal.style.display = "none";
}

window.addEventListener("click", function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
});



</script>


## AAA Settings
1.Go to **Configuration** > **Security** > **AAA** > **Radius** > **Servers** and add Cisco ISE:

 - **Name** = `ISE`
 - **Server Address** = `isePrivateIP`
 - **Key** = `ciscolive2009`
 - **Support for CoA** = `Enabled`

<div class="alert">
  <strong>Note:</strong> Both WLC and ISE are hosted on AWS. Communication between them goes over private  172.x AWS subnet, never actually reaching the internet. Plus it lowers your bandwidth cost!
</div>
<br>
<img src="ewa/ewa-05.png" width="100%">

<hr>

2.Navigate to Server Groups tab and create a new server group:

 - Name = `ISEgroup`
 - MAC-Delimiter = `colon`
 - Assigned Servers = `ISE`

<img src="ewa/ewa-06.png" width="100%">

<hr>

3.Navigate to **Configuration** > **Security** > **AAA** > **AAA Method List** > **Authentication** and create new authentication method:

 - **Name** = `EWAauth`
 - **Type** = `login`
 - **Group Type** = `group`
 - **Assigned Server Groups** = `ISEgroup`

<img src="ewa/ewa-07.png" width="100%">



## WLAN/Policy Settings

1.Under **Configuration** > **Tags & Profiles** > **WLANs**, create a new WLAN:

 - **Name** = `EWA-X`
 - **Status** = `Enabled`
    
<img src="ewa/ewa-08.png" width="100%">

<hr>

2.Under **Security** > **Layer2** set:

 - **Layer 2 Security Mode** = `None`
 - **Fast transition** = `Disabled`
 - **OWE Transition mode** = `Disabled`
    
<img src="ewa/ewa-09.png" width="100%">

<hr>

3.Under **Security** > **Layer3** set:

 - **Web Policy** = `Checked`
 - **Web Auth Parameter Map** = `EWA`
 - **Authentication List** = `EWAauth`

<img src="ewa/ewa-10.png" width="100%">

<hr>

4.Under **Configuration** > **Tags & Profiles** > **Policy** create a new Policy Profile:

- **Name** = `EWA`
- **Status** = `Enabled`

<img src="ewa/ewa-10a.png" width="100%">
  
<hr>

 - Under Access Policies, set **VLAN/VLAN Group** = `1`. If a personal AP is being used, set it to a vlan that is configured at your home/office.
    
<img src="ewa/ewa-10b.png" width="100%">
  
<hr>

5.Under **Configuration > Tags & Profiles > Tags > default-policy-tag** link the newly created WLAN `EWA-X` to the already a policy profile `EWA` and click **Update and Apply**.

<img src="ewa/ewa-11.png" width="100%">



## ISE Settings

1.Log into ISE web interface and navigate to the top left hamburger menu. Open **Administration** > **Network Resources** > **Network Devices**:

<img src="ewa/ewa-12.png" width="100%">

 - Click on the `+ Add` button:
 
<img src="ewa/ewa-13.png" width="100%">

 - Name: `9800`
 - IP Address: `wlcPrivateIP`
 - Radius Authentication Settings = `Checked`
 - Shared Secret = `ciscolive2009`
 - Make sure to navigate to the bottom of the page and click the `Submit` button.
 
 <img src="ewa/ewa-14.png" width="100%">
 <img src="ewa/ewa-15.png" width="100%">

<hr>

2.Next, we will create a user on Cisco ISE that we can log in with. Open **Administration** > **Identity Management** > **Identities**

<img src="ewa/ewa-16.png" width="100%">

 - Click on the `+ Add` button:
 
<img src="ewa/ewa-17.png" width="100%">

 - Fill in the following details:
 - Username: `ewa`
 - Password: `ciscolive2009`
 - Make sure to navigate to the bottom of the page and click the `Submit` button.
  
<img src="ewa/ewa-18.png" width="100%">

<hr>

<div class="alert">
  <strong>Note:</strong> By default, ISE will automatically authenticate all internal users via RADIUS without any configuration needed. You can verify if the default rule is there by going to the <b>Policy</b> > <b>Policy Sets</b> > <b>Default policy set</b>. Once you successfully authenticate, you should see the Hit count of the "Default" rule incrementing.
</div>

<hr>
<img src="ewa/ewa-18a.png" width="100%">

<hr>

<img src="ewa/ewa-18b.png" width="100%">

## Testing

WLAN should already be broadcasting.
Open up one of the remote wireless clients by clicking on the button below and login using credentials:

```
    Username: podX
    Password: ciscolive2009
```

Connect to the network by clicking the top right corner of the screen and selecting **Wi-Fi Settings**. Use the previously created `ewa/ciscolive2009` credentials to log into the portal:

<img src="new-lwa/lwa-15.png" width="100%">

<a style="color: white; color:visited: white" color = "white" href="guacClient" target="_blank"><button type="submit" class="btn btn-success"> Client</button></a>

<a style="color: white; color:visited: white" color = "white" href="guacClienbackup" target="_blank"><button type="submit" class="btn btn-success"> Backup client</button></a>

<hr>

The portal page should look something like this:

<img src="ewa/ewa-19.png" width="100%">