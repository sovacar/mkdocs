# 3. DNA Spaces Captive Portal

## Task

You end customer, a big shopping mall chain, has invested in cloud-based location services with Cisco DNA Spaces. Besides providing client location and visitor metrics, DNA Spaces also has the capability of serving as an external web authentication portal. Your end customer would like to utilize this to create a passthrough portal to remind customers to wear mask and socially distance. We will make use of the premade portal templates on DNA Spaces to achieve this. After the client accepts the terms and conditions on the captive portal page, they should be redirected to the shopping mall web page over at https://www.shoppingmall.co.uk.

Your job is to connect 9800 controller to DNA Spaces cloud and configure web passthrough SSID. 

## Importing root certificate

In order to establish a secure connection between DNA Spaces cloud and 9800 WLC, it is required to import a trusted DigiCert root certificate. SSH into WLC by clicking on the button below (or via your SSH client of choice) and use credentials:

<div class="alert">
  <strong>Note:</strong> It is also possible to run commands directly in the WLC web interface under <b>Administration</b> > <b>Command Line Interface</b>.
</div>

```
Username: podX
Password: ciscolive2009
```

<a style="color: white; color:visited: white" color = "white" href="guacSSH" target="_blank"><button type="submit" class="btn btn-success"> SSH via Browser!</button></a>






<style>
.alert {
  padding: 20px;
  background-color: #fdfd96;
  color: black;
}

</style>

<hr>


```
	conf t
    ip name-server 1.1.1.1
    ntp server pool.ntp.org
    ip domain lookup
    crypto pki trustpool import url https://www.cisco.com/security/pki/trs/ios.p7b

    #Reading file from http://www.cisco.com/security/pki/trs/ios.p7b
    #Loading http://www.cisco.com/security/pki/trs/ios.p7b !!!
    #%PEM files import succeeded.
```
To verify certificate has been installed, run:
```
    show crypto pki trustpool | s DigiCert Global Root CA
    cn=DigiCert Global Root CA
    cn=DigiCert Global Root CA
```

## Connecting to cloud

1.Log into [https://dnaspaces.eu](https://dnaspaces.eu) with your account (you should have received the invite before this session started) and click on the burger menu in the top left corner. Navigate to **Setup** > **Wireless Networks** > **Connect WLC/Catalyst 9800 Directly**. 


<img src="dnas/dnas-01.png" width="100%">

<hr>

Click on **View Token**. Switch tab to "**Cisco Catalyst 9800**". Copy the **URL** and **Token string**.

<img src="dnas/dnas-02.png" width="100%">
<hr>
<img src="dnas/dnas-03.png" width="100%">

<hr>

2.In 9800 web interface under **Configuration** > **Services** > **Cloud Services** > **DNA Spaces**:

 - **Enable Service** = `Checked`
 - **ServiceURL** = `https://holewn2009.dnaspaces.eu`
 - **Authentication token** = `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6Mjc3OH0.aB3LvMq2wLgntoweG_VmX5JzLLf0gX_wiebgaZmWx1c`



<img src="dnas/dnas-04.png" width="100%">

---


3.Navigate to **Monitoring** > **Wireless** > **NMSP** and make sure everything connection with the cloud is successful


<img src="dnas/dnas-05.png" width="100%">


## Investigation Trivia
 
What ports need to be open between WLC and DNA Spaces? Is there any need to perform port forwarding on the router?
	

<button type="button" class="btn btn-warning" id="myBtn">I need the answer!</button>



<style>


  /* The Modal (background) */
.modal {
 display: none; /* Hidden by default */
 position: fixed; /* Stay in place */
 z-index: 1; /* Sit on top */
 left: 0;
 top: 0;
 width: 100%; /* Full width */
 height: 100%; /* Full height */
 overflow: auto; /* Enable scroll if needed */
 background-color: rgb(0,0,0); /* Fallback color */
 background-color: rgba(0, 0, 0, 0.751); /* Black w/ opacity */
}


/* Modal Content/Box */
.modal-content {
 background-color: #fefefe;
 margin-top: 25%;
 margin-bottom: 5%;
 margin-left: 320px;
 margin-right: 20%;
 padding: 20px;
 border: 1px solid #888;
 border-radius: 15px;
}

/* The Close Button */
.close {
 color: #aaa;
 float: right;
 font-size: 28px;
 font-weight: bold;
}

.close:hover,
.close:focus {
 color: black;
 text-decoration: none;
 cursor: pointer;
} 
</style>



<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">

    <span class="close">&times;</span>
    <p>For our basic web passthrough SSID, DNA Spaces and WLC need to be able to communicate over <b>HTTP (port 80)</b> and <b>HTTPS (port 443)</b>. However, DNA Spaces can also function as a radius server. In this case, they will also talk over ports <b>1812</b> and <b>1813</b>.
    <hr>
    <p>There is no need to perform any port forwarding on the WLC side. WLC can sit behind a NAT and still happily communicate with the cloud.
  </div>
<br>

</div> 
<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
 modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
 modal.style.display = "none";
}

window.addEventListener("click", function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
});



</script>







## WLC configuration

1.Under **Configuration** > **Security** > **URL Filters** create a URL Filter:

 - **Name** = `DNASurl`
 - **Type** = `PRE-AUTH`
 - **Action** = `PERMIT`
 - **URLs** = `splash.dnaspaces.eu`

<img src="dnas/dnas-06.png" width="100%">

---


2.Under **Configuration** > **Security** > **Web Auth**, create new Web Auth parameter:

 - Name = `DNAS` 
 - Type = `consent`



<img src="dnas/dnas-07.png" width="100%">

---


3.Click on the DNAS parameter map configured in the previous step, navigate to the **Advanced** tab and add:

 - **Redirect for log-in** = `https://splash.dnaspaces.eu/p2/holewn2009`
 - This URL can be obtained in DNA Spaces under **Captive Portal** > **SSIDs** > **{ssid-name}** > **Configure Manually**
- **Redirect On-Success** = `https://www.shoppingmall.co.uk`
 - **Redirect Append for AP MAC Address** = `ap_mac`
 - **Redirect Append for Client MAC Address** = `client_mac`
 - **Redirect Append for WLAN SSID** = `wlan`
 - **Portal IPV4 Address** = `54.77.207.183`
 - Any address that the splash.dnaspaces.eu resolves to can be placed under the Portal IPV4 Address field.


<div class="alert">
  <strong>Note:</strong> By Specifying this Portal IPV4 Address, the WLC will automatically create 2 ACLs: <b>WA-sec-54.77.207.183</b> and <b>WA-v4-int-54.77.207.183</b> which specify the type of traffic that will be allowed/denied and intercepted. DNA Spaces portal is hosted on AWS and the domain splash.dnaspaces.eu resolves to multiple addresses. By specifying a URL Filter, the WLC will dynamically expand these ALCs to allow/deny traffic that the domain splash.dnaspaces.eu resolves to.
</div>

<hr>
<img src="dnas/dnas-08.png" width="100%">

<hr>


4.Under **Configuration** > **Tags & Profiles** > **WLANs** add a new WLAN:

 - **Name** = `DNAS-X`
 - **Status** = `Enabled`

<img src="dnas/dnas-09.png" width="100%">

<hr>

 - Under Layer2 Security, set **Security mode** = `None`
 - **MAC Filtering** = `Disabled`
 - **OWE Transition Mode** = `Disabled`

<img src="dnas/dnas-10.png" width="100%">

<hr>

 - Under Layer 3 Security, set **Web Policy** = `Enabled`
 - **Web Auth Parameter Map** = `DNAS`

<img src="dnas/dnas-11.png" width="100%">



5.Under **Configuration** > **Tags & Profiles** > **Policy** create new Policy profile:

 - **Name** = `DNAS`
 - **Status** = `Enabled`

<img src="dnas/dnas-12.png" width="100%">

<hr>

- Under Access Policies, set **VLAN/VLAN Group** = `1`. If a personal AP is being used, set it to a VLAN that is configured at your home/office.
 - **Pre Auth URL FIlter** = `DNASurl`

<img src="dnas/dnas-13.png" width="100%">

<hr>

6.Under **Configuration** > **Tags & Profiles** > **Tags**, under **default-policy-tag** link the newly created DNAS wlan and DNAS policy profile:

<img src="dnas/dnas-14.png" width="100%">













## DNA Spaces Captive Portal config

1.On [https://dnaspaces.eu](https://dnaspaces.eu) open up the captive portal app:

<img src="dnas/dnas-15.png" width="100%">

<hr>

2.Lets clone the existing **Instant Portal - Stay Safe White** template by clicking on it:

<img src="dnas/dnas-16.png" width="100%">

<hr>

3.Click on the `Duplicate this portal` at the bottom of the page:

<img src="dnas/dnas-17.png" width="100%">

<hr>

4.Set the **Portal Name** = `Portal-X` and check the `Pod-X` under Location Hierarchy:

<img src="dnas/dnas-18.png" width="100%">

<hr>

5.Since this is a passthrough SSID, set **No Authentication** and check **Display Data Capture and User Agreements on portal home page**:

<img src="dnas/dnas-19.png" width="100%">

<hr>

6.No need to enable anything under data capture field. Click **Next**:

<img src="dnas/dnas-20.png" width="100%">


<hr>

7.There is no need to make changes under the User Agreements tab. Just click **Save & Configure Portal**.


<img src="dnas/dnas-21.png" width="100%">


<hr>

8.The portal should now be created and shown in the list. You may also see other portals created by other lab participants since this DNA Spaces account is shared.



<hr>

9.Click on the burger menu in the top left corner and navigate to SSIDs:

<img src="dnas/dnas-23.png" width="100%">

<hr>

10.Click **Import/Configure SSID**:

<img src="dnas/dnas-24.png" width="100%">

<hr>

11.Select `Cisco AireOS/Cisco Catalyst 9800` from the dropdown menu and enter the SSID name. 

<div class="alert">
  <strong>Important:</strong> SSID name needs to be exactly the same as on the WLC. If there is an SSID name mismatch between cloud and WLC, the portal will not be presented to the client device. 
</div>

<hr>

<img src="dnas/dnas-25.png" width="100%">

12.Click on the burger menu in the top left corner and go to captive portal rules and click `Create New Rule`:


<hr>

<img src="dnas/dnas-26.png" width="100%">

<img src="dnas/dnas-26a.png" width="100%">

<hr>

 - **Rule Name** = `RuleX`
 - **When a user is on WiFi and connected to** = `DNAS-X`
 - **Location** = `Pod-X`
 - Under Actions, select **Show Captive Portal** = `Portal-X`


<hr>

<img src="dnas/dnas-27.png" width="100%">
<img src="dnas/dnas-28.png" width="100%">







## Testing

WLAN should already be broadcasting.
Open up one of the remote wireless clients by clicking on the button below and login using credentials:

```
    Username: podX
    Password: ciscolive2009
```

Connect to the network by clicking the top right corner of the screen and selecting **Wi-Fi Settings**.
<img src="new-lwa/lwa-15.png" width="100%">

<a style="color: white; color:visited: white" color = "white" href="guacClient" target="_blank"><button type="submit" class="btn btn-success"> Client</button></a>

<a style="color: white; color:visited: white" color = "white" href="guacClienbackup" target="_blank"><button type="submit" class="btn btn-success"> Backup client</button></a>


<hr>







## Homework/Bonus Points

<div class="alert">
  <strong>Note:</strong> Lab proctors will let you know when we should move to the next chapter. If you are quick, you may even have enough time to finish this bonus section as well. If not, dont worry. The lab will remain accessible for 1 day after the end of the session. 
</div>

<hr>

Shopping mall's marketing team has overheard that their competitor has managed to increase their visits by 20% by running an email newsletter about attractive discounts. They decided to implement a field in their DNA Spaces portal that will require users to enter their email address before they connect to the network. Your job is to modify the portal and implement this functionality. While you are at it, might as well replace the giant picture saying "Your Company" that you forgot to remove and replace it with the text saying "Shopping Mall Company". Hopefully no one spotted it by now.


Steps:

1. Edit the Portal-X on the DNA Spaces
2. Under Brand Name menu replace Logo with "Text Only" and enter "Shopping Mall Company"
3. Click on the Data Capture field at the top, enable email Field Element and make it mandatory.
4. Go back to the main portal page and publish the changes
5. Don't forget to turn Wi-Fi off on the wireless client and delete client from WLC before attempting to test the new changes


<hr>
