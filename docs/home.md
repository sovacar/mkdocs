# Home

## Premise
You are a new network engineer working for a company that provides wireless services to several end customers. You recently learned a couple of things about Web Authentication and think you have what it takes to take on the new projects coming in. 

## About this lab

In this lab, you will be tasked to configure 4 different web authentication SSIDs on 9800 Wireless LAN Controller running on AWS cloud. Cisco 1815 and 1852 Access Points have been joined to this WLC from 2 different remote locations. All the APs have been assigned to the default policy tag and default flex tag. There should be no need to change these. All the WLANs in this lab will be locally switched. 

All 9800 WLCs are running latest 17.3.3 code, while Cisco ISE (also hosted on AWS) is running 3.0 code.

We offer the possibility to connect your own remote home AP to the controllers. It is not mandatory, but it can be just a bit more fun to try with your own AP and client device.

To test out the deployed SSIDs, all the participants are provided with 2 remote wireless clients (main + backup). The backup one is there just in a case something goes wrong with the main one. It also might be a bit slower.

There is no software requirements for this lab. Everything can be performed through the browser, including SSH-ing into WLC and using remote wireless clients.

Our estimate is that this lab can be completed within 3-3.5 hours. If you are a bit slower, don't worry! Access to all the lab devices will remain for 1 day after the end of the session.

<style>
.alert {
  padding: 20px;
  background-color: #fdfd96;
  color: black;
}

</style>

<div class="alert">
  <strong>Important:</strong> Make sure to follow the exact naming convention specified in this lab. Your Pod number is specified in the top left corner. If, by any chance mix up your pod number and/or SSID name, you may end up accidentally connecting to someone else's setup.
</div>

<hr>

## Network Diagram

<img src="new-lwa/diagram.png" width="100%">

<hr>