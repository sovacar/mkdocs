# 1. Local Web Auth (LWA)

Your plan for this project mostly consists of stealing those config notes your colleagues made when configuring similar things for previous projects, it must not that be that hard!

<div class="alert">
  <strong>Note from Cisco Live proctors:</strong> This means that the instructions will typically miss one or two small steps to be completely working. The troubleshooting section will give you the tools to troubleshoot what happens when things are not working. Of course the solution is always there if you are out of time or patience. The goal is to learn :)
</div>
<br>




## Task

Your boss has asked you to configure a web authentication SSID. The end customer does not like the default web authentication page and wants to use a customized one. They cannot afford a AAA server, nor a separate web server to host the page, so web page should be hosted on the WLC and client authentication should be done directly on the controller. Guests should log in using username and passwords created for them by receptionist. Receptionist should be able to create guest accounts for visitors, but should not be able to change any other WLC configuration. The guest accounts should not be valid for more than 1 week. The WLC has already been provisioned with a trusted, signed certificate.

## Basic LWA config

1.Under **Configuration** > **Security** > **Web Auth** > **global** define global webauth parameters:

- **Virtual IPv4 Address** = `192.0.2.1`
- **Trustpoint** = `final.pfx`
- **Virtual IPv4 Hostname** = `vip.thewifilab.net`

<img src="new-lwa/lwa-01.png" width="100%">

Virtual IP addresses are not mandatory in all cases, but are required in some. It is a best practice to configure them right away for web auth deployments to go smoother. The WLC has been provisioned with a certificate, so specifying the Virtual IPv4 hostname is mandatory if we want the portal page not to throw certificate error. 


<div class="alert">
  <strong>Note:</strong> Keep in mind that the DNS entry `vip.thewifilab.net` on your DNS server should be pointing to WLC virtual IP address! This is already preconfigured in our lab.
</div>
<br>


The next tasks creates a named web parameter map, but some settings (like the ones we changed above) are only available in the global parameter map (only one certificate and one virtual IP can be used for all SSIDs on the WLC)

<hr>

2.Create a new parameter for our local webauth SSID:

- **Name** = `LWA`
- **Type** = `webauth`

<style>
.alert {
  padding: 20px;
  background-color: #fdfd96;
  color: black;
}

</style>



<img src="new-lwa/lwa-02.png" width="100%">

<hr>

3.Under **Configuration** > **Tags & Profiles** > **WLANs**, create a new WLAN:

- **Name** = `LWA-X`
- **Status** = `ENABLED`
  
<div class="alert">
  <strong>Note:</strong> Make sure to use this exact SSID name.
</div>
<br>

<img src="new-lwa/lwa-03.png" width="100%">

<hr>

- Under **Security** > **Layer2**, set Layer 2 = `None`
- **Fast transition** = `DISABLED`
- **OWE Transition mode** = `DISABLED`

	<img src="new-lwa/lwa-04.png" width="100%">
  
<hr>

- Under **Security** > **Layer3** enable Web Policy
- **Web Auth Parameter Map** = `LWA`
- Click on **Apply to Device** to save the WLAN configuration

	<img src="new-lwa/lwa-05.png" width="100%">

<hr>

4.Under **Configuration** > **Tags & Profiles** > **Policy** create a new Policy Profile:

- **Name** = `LWA`
- **Status** = `Enabled`
- **Central Authentication** = `Enabled`

	<img src="new-lwa/lwa-06.png" width="100%">
  
<hr>

- Under Access Policies, set **VLAN/VLAN Group** = `1`. If a personal AP is being used, set it to a VLAN that is configured at your home/office.
    
	<img src="new-lwa/lwa-07.png" width="100%">
  
<hr>

- Under **Advanced**, set **Session Timeout** = `86400` (24 hours). We don't want our guests disconnecting all the time
- **Idle timeout** = `1800` (30 minutes)
- **IPv4 DHCP Required** = `Enabled`
- Click on **Apply** to Device to save the Policy configuration

	<img src="new-lwa/lwa-08.png" width="100%">
  
<hr>

5.Under **Configuration** > **Tags & Profiles** > **Tags** > **default-policy-tag** link the newly created WLAN and Policy Profile and click Update and Apply. SSID should start broadcasting now.

<img src="new-lwa/lwa-09.png" width="100%">


## Lobby Admin

1.Before we test out the SSID, lets create lobby admin user and make sure it works correctly. Navigate to **Administration** > **User Administration** and create a new user:

- **User Name** = `lobby`
- **Policy** = `None`
- **Privilege** = `Lobby Admin`
- **Password** = `ciscolive2009`


<img src="new-lwa/lwa-10a.png" width="100%">

<hr>

<img src="new-lwa/lwa-10b.png" width="100%">

<hr>

2.In order to test out if lobby user is able to create guest accounts, log out of the WLC by clicking the button in the top right corner and log in with a newly created "lobby" user. Create a new guest user:
   
- **User Name** = `lwa`
- **Password** = `ciscolive2009`
- **Lifetime** = `7 days`
  
	<img src="new-lwa/lwa-11.png" width="100%">

<hr>

3.Log out of the WLC web interface and log back in with the `podX/ciscolive2009` credentials.

## Investigation Trivia
 
What privilege level is the lobby admin have, if any? Click to find out!
	

<button type="button" class="btn btn-warning" id="myBtn">I need the answer!</button>


<style>


  /* The Modal (background) */
.modal {
 display: none; /* Hidden by default */
 position: fixed; /* Stay in place */
 z-index: 1; /* Sit on top */
 left: 0;
 top: 0;
 width: 100%; /* Full width */
 height: 100%; /* Full height */
 overflow: auto; /* Enable scroll if needed */
 background-color: rgb(0,0,0); /* Fallback color */
 background-color: rgba(0, 0, 0, 0.751); /* Black w/ opacity */
}


/* Modal Content/Box */
.modal-content {
 background-color: #fefefe;
 margin-top: 25%;
 margin-bottom: 5%;
 margin-left: 320px;
 margin-right: 20%;
 padding: 20px;
 border: 1px solid #888;
 border-radius: 15px;
}

/* The Close Button */
.close {
 color: #aaa;
 float: right;
 font-size: 28px;
 font-weight: bold;
}

.close:hover,
.close:focus {
 color: black;
 text-decoration: none;
 cursor: pointer;
} 
</style>



<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">

    <span class="close">&times;</span>
    <p>Lobby admins use privilege level 15!
    <hr>
    <p>If you are using RADIUS or TACACS to authenticate lobby admin user, don't forget to add the <code>remote user</code> command on the WLC!</p>
  </div>
<br>

</div> 
<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
 modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
 modal.style.display = "none";
}

window.addEventListener("click", function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
});



</script>
























## Task Plot Twist

Your boss ended up giving a promotion to one of your colleague who totally didn't deserve it instead of giving it to you. Show those guys the sweet taste of revenge. Make their custom webauth page background pink!

## Custom Webauth Bundle

Cisco offers custom webauth bundle, a collection of HTML pages with small pieces of javascript code that can be edited in order to customize the portal page. Engineers can edit these to fit their use-case (web auth page, passthrough page, etc..).


1.Download the webauth bundle here:
   

<form action="wlc_webauth_bundle_1.0.zip" method="get" target="_blank">
  <button type="submit" class="btn btn-info">Download bundle!</button>
</form>
<br>




2.Unzip the bundle and navigate to **custom_webauth** folder. Open `login.html` page with your prefered text editor. Background color of the page can be set to pink by altering a piece of code in the login.html page:

```
<body bgcolor=#FFEECC topmargin="50" marginheight="50" onload="loadAction();">
```
becomes: 

```
<body bgcolor="pink" topmargin="50" marginheight="50" onload="loadAction();">
```

<div class="alert">
  <strong>Note:</strong> Feel free to alter any of the the existing or add new HTML tags. As long as the javascript code in the login.html remains intact, the page should function properly. 
</div>

<hr>

3.New customized `login.html` page can now be uploaded to the WLC. Navigate to **Administration** > **Management** > **File Manager** and enter bootflash. Upload the edited `login.html` file:

<div class="alert">
  <strong>Note:</strong> Make sure to upload <code>aup.html</code> file (it contains terms and conditions text) as well since it is embedded inside <code>login.html</code>. Without it the page will be distorted.
</div>
<hr>

  
<img src="new-lwa/lwa-12.png" width="100%">
<hr>
<img src="new-lwa/lwa-13.png" width="100%">
	 
<hr>


4.Go to **Configuration** > **Security** > **Web Auth**. Under `LWA` parameter advanced options, select customized `bootflash:/login.html` page:

<img src="new-lwa/lwa-14.png" width="100%">
	 



## Testing

WLAN should already be broadcasting.
Open up one of the remote wireless clients by clicking on the button below and login using credentials:

```
    Username: podX
    Password: ciscolive2009
```

Connect to the network by clicking the top right corner of the screen and selecting **Wi-Fi Settings**. Use the previously created `lwa/ciscolive2009` credentials to log into the portal:

<img src="new-lwa/lwa-15.png" width="100%">


<a style="color: white; color:visited: white" color = "white" href="guacClient" target="_blank"><button type="submit" class="btn btn-success"> Client</button></a>

<a style="color: white; color:visited: white" color = "white" href="guacClienbackup" target="_blank"><button type="submit" class="btn btn-success"> Backup client</button></a>


<hr>




## Troubleshooting

Of course, it's not working! You are facing at minimum two and possibly three issues, depending on your client.


### Issue #1

The first is, by far, the most commonly encountered problem in the guest network deployment and something that Cisco TAC gets at least couple of cases per day! The automatic redirection is not working. There are 3 short checks that need to be performed. Do you know what they are?



<button type="button" class="btn btn-danger" id="troubleshoot1btn">I need the answer!</button>


<!-- The Modal -->
<div id="troubleshoot1modal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">

    <span class="close" id ="closeBtn1" >&times;</span>
<p>1. Is the client able to obtain the IP address?
<p>Verify by running <code>ifconfig</code> on the client terminal. The client should get an address from 192.168.x.x subnet.

<hr>

<p>2. Is the client able to perform DNS queries? 
<p>When the client connects to the network, it performs automatic HTTP GET request towards predefined domain. If domain resolution doesn't work, client will not know where to send HTTP GET request automatically.

Redirection might work without functioning DNS server, but only if you manually go to http://1.2.3.4 (or some other IP) in the browser.

<p>Verify if client is able to resolve domain names by running <code>nslookup google.com</code> in the client terminal. 

<hr>

<p>3. Does WLC have its HTTP server enabled?
<p>In order for our WLC to intercept the HTTP traffic, it needs to have its http server enabled with the command <code>ip http server</code>. This command opens up the port 80 on the WLC and allows it to perform HTTP intercept.
</p> 
</p>
<b>Note</b>: Security engineers scanning the network might not like this. Even with port 80 being open, network administrators cannot access the WLC admin page via the HTTP if the <code>ip http secure-server</code> command is present (which is already there by default). The WLC will automatically redirect administrators to the HTTPS version of the page.</p>



  </div>
<br>

</div> 
<script>
// Get the modal
var modal1 = document.getElementById("troubleshoot1modal");

// Get the button that opens the modal
var btn1 = document.getElementById("troubleshoot1btn");

// Get the <span> element that closes the modal
var span1 = document.getElementById("closeBtn1");

// When the user clicks on the button, open the modal
btn1.onclick = function() {
 modal1.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span1.onclick = function() {
 modal1.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
 if (event.target == modal1) {
   modal1.style.display = "none";
 }
} 

</script>



<hr>

### Issue #2

The second one is a nasty problem that is not easy to troubleshoot and is not always reproducible. You may see the login page recursively repeating itself. Any idea what could cause it?

<img src="new-lwa/missingipv6.png" width="100%">
 


<button type="button" class="btn btn-danger" id="troubleshoot2btn">I need the answer!</button>

<!-- The Modal -->
<div id="troubleshoot2modal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">

    <span class="close" id="closeBtn2">&times;</span>
<p>

If you take PCAP on the client machine, you can see that the client is trying to reach the internet using IPv6 first and this is what triggered the redirection.
The problem is that you did not configure a virtual IPv6 address on the WLC, which can corrupt the homepage or make it not appear at all. This happens only with dual stack clients, even if you did not intend to browse using IPv6 in particular. It is also not reproducible all the time (depending on which protocol client tries to use first).
<hr>
Go back to the <b>global</b> webauth parameter map and configure virtual IPv6 address <code>FE80:0:0:0:903A::11E4</code>. Any made up IPv6 address can be used here, as long as there is no duplicate somewhere on the network.




</p> 
</p>




  </div>
<br>

</div> 
<script>
// Get the modal
var modal2 = document.getElementById("troubleshoot2modal");

// Get the button that opens the modal
var btn2 = document.getElementById("troubleshoot2btn");

// Get the <span> element that closes the modal
var span2 = document.getElementById("closeBtn2");

// When the user clicks on the button, open the modal
btn2.onclick = function() {
 modal2.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span2.onclick = function() {
 modal2.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
 if (event.target == modal2) {
   modal2.style.display = "none";
 }
} 

</script>



<hr>

### Issue #3

Authentication shows as failed even if you type valid `lwa/ciscolive2009` credentials. What did your colleague forget in that template you "took inspiration from"?

We know the client is getting the login page so redirection is now working. There is no need to go verify in <b>Monitor</b> > <b>Clients</b> that the client is correctly in "Webauth_reqd" state. It must be something revolving around authentication. The best is to collect a Radioactive trace.

Figure out the mac address of your wireless client (watch out for mac randomization) and add it under <b>Troubleshooting</b> > <b>Radioactive</b> trace page. Click start and reproduce the failed authentication to generate the logs. Notice how completely lit the Radioactive Tracing collection process got in 17.3.3.

Here is a snippet of interesting logs lines in the RA trace you should be seeing :

```
2021/03/16 18:05:33.293821 {wncd_x_R0-0}{1}: [auth-mgr] [17880]: (info): [f018.9875.2dca:capwap_90000005] Authc failure from WebAuth, Auth event fail
2021/03/16 18:05:33.293827 {wncd_x_R0-0}{1}: [auth-mgr] [17880]: (info): [f018.9875.2dca:capwap_90000005] (Re)try failed method WebAuth - f018.9875.2dca
```

**Hint**: Did you really check everything that was written on the screen in the last screenshot of step 3 during the WLAN configuration? 







<button type="button" class="btn btn-danger" id="troubleshoot3btn">I need the answer!</button>

<!-- The Modal -->
<div id="troubleshoot3modal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">

    <span class="close" id="closeBtn3">&times;</span>
<p>
Without <code>aaa authorization network default local</code> command, the WLC does not know how it should authenticate users. This command tells it to use the local user database.
SSH into WLC and apply it or simply use the built in CLI tool under <b>Administration</b> > <b>Command Line Interface</b>

</p> 





  </div>
<br>

</div> 
<script>
// Get the modal
var modal3 = document.getElementById("troubleshoot3modal");

// Get the button that opens the modal
var btn3 = document.getElementById("troubleshoot3btn");

// Get the <span> element that closes the modal
var span3 = document.getElementById("closeBtn3");

// When the user clicks on the button, open the modal
btn3.onclick = function() {
 modal3.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span3.onclick = function() {
 modal3.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
 if (event.target == modal3) {
    modal3.style.display = "none";

 }
  if (event.target == modal2) {
    modal2.style.display = "none";

 }
   if (event.target == modal1) {
    modal1.style.display = "none";

 }
    if (event.target == modal) {
    modal.style.display = "none";

 }
} 




</script>




































## Homework/Bonus Points

<div class="alert">
  <strong>Note:</strong> Lab proctors will let you know when we should move to the next chapter. If you are quick, you may even have enough time to finish this bonus section as well. If not, don't worry. The lab will remain accessible for 1 day after the end of the session. 
</div>

<hr>

End customer was worried about security on this network. They would like to implement an access list that would block authenticated guest users from accessing any of their private IPv4 address ranges (RFC 1918), while still having full access to everything else:

```
Ranges to be blocked:
192.168.0.0/16
172.16.0.0/16
10.0.0.0/16
```
This type of ACL is a called post auth ACL and is applied to wireless client only after they reach the run state.

Steps:

1. Create an ACL denying traffic to and from these IP ranges and allowing everything else
2. Assign the ACL to the `LWA` **Policy Profile**
3. Assign ACL to the default-flex-profile under Policy ACL menu, with Central Web Auth checkbox disabled


Verify if ACL is working by failing to ping `192.168.137.98` and `192.168.1.98`. 
<hr>
