# 4. Central Web Auth (CWA)

## Task

Your customer has decided to beef up their security in their 5 start hotel by implementing a BYOD SSID using Cisco Identity Service.  They weren't so happy with your previous pink portal page, so this time they specifically requested that the portal uses Fresh Blue theme that Cisco ISE offers.


<div class="alert">
  <strong>Note:</strong> Cisco ISE and 9800 integration should already be performed in during the EWA section.
</div>
<br>

## WLC Configuration

1.Navigate to **Configuration** > **Security** > **AAA** > **AAA Method List** > **Authorization** and create a new authorization profile by clicking `+ Add`:

 - **Name** = `CWAauthz`
 - **Type** = `network`
 - **Group Type** = `group`
 - **Assigned Server Groups** = `ISEgroup`

<img src="cwa/cwa-01.png" width="100%">

<hr>


2.(Optional) Under **Configuration** > **Security** > **AAA** > **AAA Method List** > **Accounting**, create a new accounting method:

 - **Name** = `CWAacct`
 - **Type** = `identity`
 - **Assigned Server Group**s = `ISEgroup`

<img src="cwa/cwa-02.png" width="100%">

<hr>



3.Under **Configuration** > **Security** > **Wireless AAA Policy** edit the existing **default-aaa-policy** to **SSID**. Everything configured under the Wireless AAA Policy will be sent over to RADIUS server inside the `Called-station-id` attribute. This will be useful if you want to leverage this condition on ISE later in the process.

<img src="cwa/cwa-03.png" width="100%">

<hr>

<style>
.alert {
  padding: 20px;
  background-color: #fdfd96;
  color: black;
}
</style>

<div class="alert">
  <strong>Note:</strong> Keep in mind that even when choosing SSID only, the Called-Station-ID will still contain AP MAC address. SSID name will be appended to it. Example of the attribute:
</div>



```
Called-Station-ID = 38-ed-18-c9-16-c0:CWA-04
```

<hr>

4.Navigate to **Configuration** > **Tags & Profiles** > **WLANs** and create new WLAN:

 - **Name** = `CWA-X`
 - **Status** = `Enabled`

<div class="alert">
  <strong>Note:</strong> Make sure to use this exact SSID name.
</div>
<br>

<img src="cwa/cwa-04.png" width="100%">


<hr>

 - **Layer 2 Security Mode** = `None`
 - **MAC Filtering** = `Enabled`
 - **Authorization List** = `CWAauthz`
 - **Fast Transition** = `Disabled`
 - **OWE transition mode** = `Disabled`

<img src="cwa/cwa-06.png" width="100%">

<hr>

5.Navigate to **Configuration** > **Tags & Profiles** > **Policy** and create a new policy:

 - **Name** = `CWA`
 - **Status** = `Enabled`
 - **Central authentication** = `Enabled`


<img src="cwa/cwa-07.png" width="100%">

<hr>

- Under Access Policies, set **VLAN/VLAN Group** = `1`. If a personal AP is being used, set it to a VLAN that is configured at your home/office.

<img src="cwa/cwa-08.png" width="100%">

<hr>

 - Under **Advanced** set  AAA Override = `Enable`
 - **NAC state** = `Enable`
 - **NAC Type** = `RADIUS`
 - **Accounting list** = `CWAacct`

<img src="cwa/cwa-09.png" width="100%">

<hr>

6.Under **Configuration** > **Tags & Profiles** > **Tags**, edit the default policy tag in order to link the newly created WLAN and policy profile:

<img src="cwa/cwa-10.png" width="100%">

<hr>

## Redirect ACL

Redirect ACLs are created on the WLC. During the L2 authentication, ISE will push the ACL name to the WLC so that it can be applied to the client. Authorization will fail if the redirect ACL is not pre-created on the WLC (and on the flex profile in case of local switching). The ACL will determine which traffic is sent towards the WLC CPU (in case of permit).

In case of traffic hitting the deny statement, it will not be sent to the WLC CPU and redirection does not occur.
Note that a deny statement will not drop it as this is a redirect ACL (also called intercept ACL or punt ACL in IOS-XE) and not a security ACL.


1.Navigate to **Configuration** > **Security** > **ACL** and create a new extended IPv4 ACL and name it `REDIRECT`.

<img src="cwa/cwa-11.png" width="100%">

<hr>

2.This redirect ACL should Deny redirection of traffic towards (and back from) ISE node's public IP address. It should end permitting HTTP. You can find the ISE public IP address by running `nslookup iseX.thewifilab.net` command in your terminal.

Not permitting HTTPS means we will not redirect HTTPS traffic, which is the recommended way. We want to rely on the automatic portal detection as much as possible, which all modern clients perform via HTTP.



<img src="cwa/cwa-12.png" width="100%">
        

## Flex profile changes

Since we have a flexconnect local switching setup, AP somehow needs to know which traffic should be sent to the WLC via CAPWAP tunnel (traffic towards ISE) and which should stay local (DHCP/DNS). By adding the redirect ACL to the default-flex-profile under **Configuration** > **Tags & Profiles** > **Flex**, the ACL is going to be pushed to the AP.

Checking **Central Web Auth** field will actually invert the permit/deny statements before pushing the ACL to the AP. This can be verified by running `show ip access-list` on the access point.

<img src="cwa/cwa-13.png" width="100%">

<hr>

## ISE Configuration

1.Log into ISE web interface and navigate to the top left hamburger menu. Open **Work Centers** > **Guest Access** > **Portals and components**:

<img src="cwa/cwa-14.png" width="100%">

<hr>
 - Click on the **Self-Registered Guest Portal**:

<img src="cwa/cwa-15.png" width="100%">

<hr>

 - Under **Portal Page Customization**, change the color of the portal to `Default Fresh Blue theme` and save:

<img src="cwa/cwa-16.png" width="100%">
  
<hr>


2. Navigate to **Policy** > **Policy Elements** > **Results** > **Authorization** > **Authorization Profiles** and create a new Profile:

<img src="cwa/cwa-17.png" width="100%">

<hr>

<img src="cwa/cwa-18.png" width="100%">

<hr>

 - **Name** = `CWAredirect`
 - **Access Type** = `ACCESS_ACCEPT`
 - **Web redirection** = `Central Webauth`
 - **ACL** = `REDIRECT`
 - **Value**= `Self-registered guest portal`
 - **Static IP/Hostname/FQDN** = `iseX.thewifilab.net` 
 
**Note:** If your ISE is hosted on AWS, every time it is shut down, the public IP is going to change. In this case, there is a script that automatically checks ISE public IP and updates the domain iseX.thewifilab.net to point to it.


<img src="cwa/cwa-19.png" width="100%">

<hr>

3.Navigate to **Policy** > **Policy Sets** and click on the default policy set arrow on the far right of the page:

<img src="cwa/cwa-20.png" width="100%">

<hr>

<img src="cwa/cwa-21.png" width="100%">

<hr>
 - Expand the **Authorization Policy** at the bottom of the list
 - Create 2 new authorization Policies:
 
	 - Rule 1 (above):
    	- **Name** = `Guest-Flow` 
			- **Condition1**: `Wireless_MAB` (pre-built)
			- **Condition2**: `Guest-flow` (pre-built)
			- **Result**: `Permit Access`
            - This rule will be hit for the second authentication taking place after the CoA which is a new mac filter authentication, but this time ISE remembers the user just logged in on the portal and remembers the username/password used.

    <img src="cwa/cwa-22.png" width="100%">

	 - Rule 2 (below):
    	- **Name** = `Redirect-to-Portal` 
			- **Condition1**: `Wireless_MAB` (pre-built)
			- **Condition2**: `Radius-Called-Station-ID` `CONTAINS` `CWA`
			- **Result**: `CWAredirect`
            - You can use the pre-built condition "Wireless_MAB" and add logical AND for the Radius Called-Station-ID to contain the SSID name. The returned result should be the previously created authorization result "CWA-result". This rule will be hit during the first authetnication of new clients (which is a pure MAC filter authentication)
 
 
<div class="alert">
  <strong>Note:</strong> The called station id does not exist in the pre-built condition library on the left side. You need to click <b>NEW</b> in the condition editor and pick "Called-Station-ID" under <b>DEVICE</b> menu
</div>



<img src="cwa/cwa-23.png" width="100%">


<hr>
Finally, the Authorization Policies should look like this:

<img src="cwa/cwa-24.png" width="100%">

<hr>

## Initial Testing

WLAN should already be broadcasting. Better to test things before the config gets more complicated, just to make sure all is right.   Open up one of the remote wireless clients by clicking on the button below and login using credentials:

```
    Wireless client credentials:
    Username: podX
    Password: ciscolive2009
```

Connect to the network by clicking the top right corner of the screen and selecting **Wi-Fi Settings**. 

<div class="alert">
  <strong>Note:</strong> In order to log into the CWA portal, use the following credentials which have already been pre-configured on ISE:
<p>
<p>Username: <code>cwa</code>
<p>Password: <code>ciscolive2009</code>
</div>

<hr>

<img src="new-lwa/lwa-15.png" width="100%">

<a style="color: white; color:visited: white" color = "white" href="guacClient" target="_blank"><button type="submit" class="btn btn-success"> Client</button></a>

<a style="color: white; color:visited: white" color = "white" href="guacClienbackup" target="_blank"><button type="submit" class="btn btn-success"> Backup client</button></a>


<hr>



## Trivia #1

Imagine the following scenario:

After hours of tinkering, you are 100% sure that your ISE and WLC are configured by the book. You are sure that there is nothing blocking traffic between WLC and ISE. But still, for some reason the client fails to connect to the network. Surprisingly, you don't even see any logs for your client on ISE under **Operations** > **Radius Live Logs**.

What troubleshooting steps would you take here? Click the button to find out!

<button type="button" class="btn btn-warning" id="myBtn">I need the answer!</button>


<style>


  /* The Modal (background) */
.modal {
 display: none; /* Hidden by default */
 position: fixed; /* Stay in place */
 z-index: 1; /* Sit on top */
 left: 0;
 top: 0;
 width: 100%; /* Full width */
 height: 100%; /* Full height */
 overflow: auto; /* Enable scroll if needed */
 background-color: rgb(0,0,0); /* Fallback color */
 background-color: rgba(0, 0, 0, 0.751); /* Black w/ opacity */
}
.modalX {
 display: none; /* Hidden by default */
 position: fixed; /* Stay in place */
 z-index: 1; /* Sit on top */
 left: 0;
 top: 0;
 width: 100%; /* Full width */
 height: 100%; /* Full height */
 overflow: auto; /* Enable scroll if needed */
 background-color: rgb(0,0,0); /* Fallback color */
 background-color: rgba(0, 0, 0, 0.751); /* Black w/ opacity */
}

/* Modal Content/Box */
.modalX-content {
 background-color: #fefefe;
 margin-top: 5%;
 margin-bottom: 5%;
 margin-left: 320px;
 margin-right: 20%;
 padding: 20px;
 border: 1px solid #888;
 border-radius: 15px;
}

/* Modal Content/Box */
.modal-content {
 background-color: #fefefe;
 margin-top: 25%;
 margin-bottom: 5%;
 margin-left: 320px;
 margin-right: 20%;
 padding: 20px;
 border: 1px solid #888;
 border-radius: 15px;
}

/* The Close Button */
.close {
 color: #aaa;
 float: right;
 font-size: 28px;
 font-weight: bold;
}

.close:hover,
.close:focus {
 color: black;
 text-decoration: none;
 cursor: pointer;
} 
</style>



<!-- The Modal -->
<div id="myModal" class="modalX">

  <!-- Modal content -->
  <div class="modalX-content">

    <span class="close">&times;</span>

<h3>1. Packet captures on either WLC or ISE:</h3>
<p>This is probably the most valuable troubleshooting technique and something TAC uses all the time since it provides insight  into the exact packet flow. WLC captures can be performed with couple of clicks in the  web interface under <b>Troubleshooting</b> > <b>Packet Capture</b>. Unfortunately, packet capturing on our 9800 WLC running on AWS is not possible.

<hr>

<h3>2. Radioactive traces:</h3>
<p>RA traces can easily provide you with in-depth information about the client onboarding flow and give you a hint on where it fails. You can even see all the RADIUS attributes in them.

<hr>

<h3>3. RADIUS Suppression  & Reports settings on ISE</h3>

<p>By default, Cisco ISE will start rejecting clients that fail authentication way too many times in a row and suppress those events from the RADIUS Live Logs. During the initial deployment of the SSID, it is very common for a client to fail authentication multiple times. ISE sometimes suppresses logs even when the authentication is successful.

<p>Empty Live Logs can often lead network administrator to falsely believe that the packets are being dropped somewhere between WLC and ISE. It is recommended to turn off suppression during the testing phase. Make sure to turn it back on once everything is up and running.</p>

<img src="cwa/cwa-24a.png" width="100%">

  </div>
<br>

</div> 
<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
btn.onclick = function() {
 modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
 modal.style.display = "none";
}

window.addEventListener("click", function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
});



</script>


<hr>


## Rate limiting

The customer requested a 2Mbps rate limit, which is completely outrageous for a 5 star hotel, but they are the customer so you have to comply. Lets go and ruin someone's late night funny cat video binge session. 


On the WLC, go to **Configuration** > **Services** > **QoS** and click **Add**. Call the QoS policy `ratelimit` and set the default category to have a Police(kbps) = `2000`. Notice that the rate limit could be application specific as well, but we will not be testing that in this Cisco Live hands on lab.

<img src="cwa/cwa-25.png" width="100%">

<hr>

 - Click Update and apply. Do not assign it to a policy profile. We want ISE to do that automatically.

 - On ISE, navigate to **Policy** > **Policy Elements** > **Results** > **Authorization** > **Authorization Profiles** and create a new Profile:

<img src="cwa/cwa-26.png" width="100%">


<hr>

We will create an authorization result that will, on top of the current permit access, return a QoS profile name to assign a bandwidth  restriction. This way, the hotel could have specific premium guest users which can later be assigned another bandwidth  contract.

 - Under the **Advanced Attributes** settings, add 2 Cisco AV-pairs:
 
    -   `ip:sub-qos-policy-in=ratelimit`
    -   `ip:sub-qos-policy-out=ratelimit`


<img src="cwa/cwa-27.png" width="100%">

<hr>

 - Go back to the authorization policy in the policy sets and change the guest flow rule to return the newly created authorization result instead of a simple permit access:
 
<img src="cwa/cwa-28.png" width="100%">

<hr>

## Verify QoS policy

Once the client connects to the network and logs into the portal, the ISE will tell the WLC to apply the rate limiting QoS profile to it. Verify that the QoS policy is applied to the client by checking the client entry details in **Monitor** > **Clients**.

## Trivia #2

The customer is now complaining that the portal page shows up every time they connect to the SSID. They would like users to not be bothered with the portal for at least 24 hours. What are the solutions?

<button type="button" class="btn btn-warning" id="troubleshoot3btn">I need the answer!</button>

<!-- The Modal -->
<div id="troubleshoot3modal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">

    <span class="close" id="closeBtn3">&times;</span>
<p>
One solution would be to set the idle timeout of the policy profile to 24 hours. This means that the client entry will persist on the WLC for up to 24 hours even after the client disconnects. The result is that if the client reconnects within this time period, it does not need to reauthenticate as its client entry is still present and in RUN state. Not the prettiest solution but it can get the job done.
</p>

  </div>
<br>

</div> 
<script>
// Get the modal
var modal3 = document.getElementById("troubleshoot3modal");

// Get the button that opens the modal
var btn3 = document.getElementById("troubleshoot3btn");

// Get the <span> element that closes the modal
var span3 = document.getElementById("closeBtn3");

// When the user clicks on the button, open the modal
btn3.onclick = function() {
 modal3.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span3.onclick = function() {
 modal3.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
 if (event.target == modal3) {
    modal3.style.display = "none";

 }
  if (event.target == modal2) {
    modal2.style.display = "none";

 }
   if (event.target == modal1) {
    modal1.style.display = "none";

 }
    if (event.target == modal) {
    modal.style.display = "none";

 }
} 


</script>