import os




guacSSH = [ "https://guac.thewifilab.net/#/client/MTgAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjAAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MTkAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjEAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjIAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjMAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjQAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjUAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjYAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjcAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjgAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjkAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MzAAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MzEAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MzIAYwBwb3N0Z3Jlc3Fs" \
        ]

guacClient = [  "https://guac.thewifilab.net/#/client/MQBjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/MgBjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/NABjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/NQBjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/NgBjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/NwBjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/OABjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/OQBjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/MTAAYwBwb3N0Z3Jlc3Fs", \
                "https://guac.thewifilab.net/#/client/MTEAYwBwb3N0Z3Jlc3Fs", \
                "https://guac.thewifilab.net/#/client/MTIAYwBwb3N0Z3Jlc3Fs", \
                "https://guac.thewifilab.net/#/client/MTMAYwBwb3N0Z3Jlc3Fs", \
                "https://guac.thewifilab.net/#/client/MTQAYwBwb3N0Z3Jlc3Fs", \
                "https://guac.thewifilab.net/#/client/MTUAYwBwb3N0Z3Jlc3Fs", \
                "https://guac.thewifilab.net/#/client/MTYAYwBwb3N0Z3Jlc3Fs" \
        ]

guacClienbackup = ["https://backup.guac.thewifilab.net/#/client/MgBjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/MwBjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/NABjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/NQBjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/NgBjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/NwBjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/OABjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/OQBjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/MTAAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTEAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTIAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTMAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTQAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTUAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTYAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTcAYwBwb3N0Z3Jlc3Fs" \
        ]




wlcPrivateIP = [    "172.31.25.38", \
                    "172.31.27.148", \
                    "172.31.25.215", \
                    "172.31.22.27", \
                    "172.31.22.15", \
                    "172.31.18.193", \
                    "172.31.18.81", \
                    "172.31.31.54", \
                    "172.31.28.90", \
                    "172.31.28.247", \
                    "172.31.24.168", \
                    "172.31.19.71", \
                    "172.31.23.28", \
                    "172.31.31.57", \
                    "172.31.18.22", \
                    "172.31.28.81" \
        ]



isePrivateIP = [    "172.31.31.114", \
                    "172.31.26.232", \
                    "172.31.16.67", \
                    "172.31.18.68", \
                    "172.31.18.46", \
                    "172.31.31.207", \
                    "172.31.24.226", \
                    "172.31.24.92", \
                    "172.31.30.214", \
                    "172.31.27.35", \
                    "172.31.26.184", \
                    "172.31.29.127", \
                    "172.31.21.229", \
                    "172.31.25.104", \
                    "172.31.25.166", \
                    "172.31.31.206" \
        ]
        
#################### home page generator
fout = [0]*15
for i in range(15):
    #fout[i] = open("home" + str(i+1) + ".html", "w+")
    fout[i] = open("/var/www/html/pod" + str(i+1) + "/home.html", "w+")


for i in range(15):
    
    #output file to write the result to
    fin = open("site/home.html", "rt")
    #fout[i] = open("pod" + str(i+1) + ".html", "w+")
    
    #for each line in the input file
    for line in fin:
        #read replace the string and write to output file
        fout[i].write(line.\
        
        replace('Pod-X', 'Pod ' + str(i+1)) \
        )
    fout[i].close()
    fin.close()
        #close input and output files


########################################
############## LWA generator
fout = [0]*15
for i in range(15):
    #fout[i] = open("LWA" + str(i+1) + ".html", "w+")
    fout[i] = open("/var/www/html/pod" + str(i+1) + "/LWA.html", "w+")


for i in range(15):
    
    #output file to write the result to
    fin = open("site/LWA.html", "rt")
    #fout[i] = open("pod" + str(i+1) + ".html", "w+")
    
    #for each line in the input file
    for line in fin:
        #read replace the string and write to output file
        fout[i].write(line.\
        
        replace('Pod-X', 'Pod ' + str(i+1)). \
        replace('guacClient', guacClient[i]). \
        replace('guacClienbackup', guacClienbackup[i]). \
        replace('podX', 'pod' + str(i+1)). \
        replace('LWA-X', 'LWA-' + str(i+1)) \
        )
    fout[i].close()
    fin.close()
        #close input and output files


########################################
############## EWA generator
fout = [0]*15
for i in range(15):
    #fout[i] = open("EWA" + str(i+1) + ".html", "w+")
    fout[i] = open("/var/www/html/pod" + str(i+1) + "/EWA.html", "w+")


for i in range(15):
    
    #output file to write the result to
    fin = open("site/EWA.html", "rt")
    #fout[i] = open("pod" + str(i+1) + ".html", "w+")
    
    #for each line in the input file
    for line in fin:
        #read replace the string and write to output file
        fout[i].write(line.\
        
        replace('Pod-X', 'Pod ' + str(i+1)). \
        replace('guacClient', guacClient[i]). \
        replace('guacClienbackup', guacClienbackup[i]). \
        replace('podX', 'pod' + str(i+1)). \
        replace('EWA-X', 'EWA-' + str(i+1)). \
        replace('isePrivateIP', isePrivateIP[i]). \
        replace('wlcPrivateIP', wlcPrivateIP[i]) \
        )
    fout[i].close()
    fin.close()
        #close input and output files



########################################
############## DNAS generator
fout = [0]*15
for i in range(15):
    #fout[i] = open("DNAS" + str(i+1) + ".html", "w+")
    fout[i] = open("/var/www/html/pod" + str(i+1) + "/DNAS.html", "w+")


for i in range(15):
    
    #output file to write the result to
    fin = open("site/DNAS.html", "rt")
    #fout[i] = open("pod" + str(i+1) + ".html", "w+")
    
    #for each line in the input file
    for line in fin:
        #read replace the string and write to output file
        fout[i].write(line.\
        
        replace('Pod-X', 'Pod ' + str(i+1)). \
        replace('guacClient', guacClient[i]). \
        replace('guacClienbackup', guacClienbackup[i]). \
        replace('podX', 'pod' + str(i+1)). \
        replace('DNAS-X', 'DNAS-' + str(i+1)). \
        replace('isePrivateIP', isePrivateIP[i]). \
        replace('wlcPrivateIP', wlcPrivateIP[i]). \
        replace('guacSSH', guacSSH[i]). \
        replace('RuleX', 'Rule' + str(i+1)). \
        replace('Portal-X', 'Portal-' + str(i+1)) \
        )
    fout[i].close()
    fin.close()
        #close input and output files


########################################
############## CWA generator
fout = [0]*15
for i in range(15):
    #fout[i] = open("CWA" + str(i+1) + ".html", "w+")
    fout[i] = open("/var/www/html/pod" + str(i+1) + "/CWA.html", "w+")



for i in range(15):
    
    #output file to write the result to
    fin = open("site/CWA.html", "rt")
    #fout[i] = open("pod" + str(i+1) + ".html", "w+")
    
    #for each line in the input file
    for line in fin:
        #read replace the string and write to output file
        fout[i].write(line.\
        
        replace('Pod-X', 'Pod ' + str(i+1)). \
        replace('guacClient', guacClient[i]). \
        replace('guacClienbackup', guacClienbackup[i]). \
        replace('podX', 'pod' + str(i+1)). \
        replace('iseX', 'ise' + str(i+1)). \
        replace('CWA-X', 'CWA-' + str(i+1)) \
        )
    fout[i].close()
    fin.close()
        #close input and output files
