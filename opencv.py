import cv2

wlcPrivateIP = [    "172.31.31.113", \
                    "172.31.27.148", \
                    "172.31.25.215", \
                    "172.31.22.27", \
                    "172.31.22.15", \
                    "172.31.18.193", \
                    "172.31.18.81", \
                    "172.31.31.54", \
                    "172.31.28.90", \
                    "172.31.28.247", \
                    "172.31.24.168", \
                    "172.31.19.71", \
                    "172.31.23.28", \
                    "172.31.31.57", \
                    "172.31.18.22", \
                    "172.31.28.81" \
        ]



isePrivateIP = [    "172.31.21.124", \
                    "172.31.26.6", \
                    "172.31.26.116", \
                    "172.31.20.209", \
                    "172.31.23.134", \
                    "172.31.27.198", \
                    "172.31.31.107", \
                    "172.31.16.218", \
                    "172.31.16.60", \
                    "172.31.26.89", \
                    "172.31.29.63", \
                    "172.31.20.91", \
                    "172.31.19.94", \
                    "172.31.30.100", \
                    "172.31.21.52", \
                    "172.31.30.90" \
        ]

for i in range(15):

    imgDir = "/var/www/html/pod" + str(i+1) + "/new-lwa/"
    imgName = "lwa-03.png"
    img = cv2.imread(imgDir + imgName, cv2.IMREAD_COLOR)

    final = cv2.putText(img, text='LWA-' + str(i+1), org=(350,327),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
    
    final = cv2.putText(img, text='LWA-' + str(i+1), org=(350,371),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
    
    cv2.imwrite("/var/www/html/pod" + str(i+1) + "/new-lwa/" + imgName ,img)



    imgDir = "/var/www/html/pod" + str(i+1) + "/ewa/"
    imgName = "ewa-05.png"
    img = cv2.imread(imgDir + imgName, cv2.IMREAD_COLOR)

    final = cv2.putText(img, text=isePrivateIP[i], org=(453,321),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.65, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
    
    cv2.imwrite("/var/www/html/pod" + str(i+1) + "/ewa/" + imgName ,img)


    imgDir = "/var/www/html/pod" + str(i+1) + "/ewa/"
    imgName = "ewa-08.png"
    img = cv2.imread(imgDir + imgName, cv2.IMREAD_COLOR)

    final = cv2.putText(img, text='EWA-' + str(i+1), org=(289,260),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
    
    final = cv2.putText(img, text='EWA-' + str(i+1), org=(289,304),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
    
    cv2.imwrite("/var/www/html/pod" + str(i+1) + "/ewa/" + imgName ,img)



    imgDir = "/var/www/html/pod" + str(i+1) + "/ewa/"
    imgName = "ewa-11.png"
    img = cv2.imread(imgDir + imgName, cv2.IMREAD_COLOR)

    final = cv2.putText(img, text='EWA-' + str(i+1), org=(576,514),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
    
    cv2.imwrite("/var/www/html/pod" + str(i+1) + "/ewa/" + imgName ,img)



    imgDir = "/var/www/html/pod" + str(i+1) + "/ewa/"
    imgName = "ewa-14.png"
    img = cv2.imread(imgDir + imgName, cv2.IMREAD_COLOR)

    final = cv2.putText(img, text=wlcPrivateIP[i], org=(300,342),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.65, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
    
    cv2.imwrite("/var/www/html/pod" + str(i+1) + "/ewa/" + imgName ,img)

#####################################################################################


    imgDir = "/var/www/html/pod" + str(i+1) + "/dnas/"
    imgName = "dnas-09.png"
    img = cv2.imread(imgDir + imgName, cv2.IMREAD_COLOR)

    final = cv2.putText(img, text='DNAS-' + str(i+1), org=(240,285),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
    
    final = cv2.putText(img, text='DNAS-' + str(i+1), org=(240,330),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
    
    cv2.imwrite("/var/www/html/pod" + str(i+1) + "/dnas/" + imgName ,img)


    imgDir = "/var/www/html/pod" + str(i+1) + "/dnas/"
    imgName = "dnas-14.png"
    img = cv2.imread(imgDir + imgName, cv2.IMREAD_COLOR)

    final = cv2.putText(img, text='DNAS-' + str(i+1), org=(565,548),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
    
    
    cv2.imwrite("/var/www/html/pod" + str(i+1) + "/dnas/" + imgName ,img)


    imgDir = "/var/www/html/pod" + str(i+1) + "/dnas/"
    imgName = "dnas-18.png"
    img = cv2.imread(imgDir + imgName, cv2.IMREAD_COLOR)

    final = cv2.putText(img, text='Portal ' + str(i+1), org=(35,184),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
    
    
    cv2.imwrite("/var/www/html/pod" + str(i+1) + "/dnas/" + imgName ,img)




    imgDir = "/var/www/html/pod" + str(i+1) + "/dnas/"
    imgName = "dnas-25.png"
    img = cv2.imread(imgDir + imgName, cv2.IMREAD_COLOR)

    final = cv2.putText(img, text='DNAS-' + str(i+1), org=(357,172),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
    
    
    cv2.imwrite("/var/www/html/pod" + str(i+1) + "/dnas/" + imgName ,img)


    imgDir = "/var/www/html/pod" + str(i+1) + "/dnas/"
    imgName = "dnas-27.png"
    img = cv2.imread(imgDir + imgName, cv2.IMREAD_COLOR)

    final = cv2.putText(img, text='Rule ' + str(i+1), org=(478,40),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)

    final = cv2.putText(img, text='DNAS-' + str(i+1), org=(416,196),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)

    final = cv2.putText(img, text='Pod-' + str(i+1), org=(107,471),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.5, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
    
    cv2.imwrite("/var/www/html/pod" + str(i+1) + "/dnas/" + imgName ,img)



    imgDir = "/var/www/html/pod" + str(i+1) + "/dnas/"
    imgName = "dnas-28.png"
    img = cv2.imread(imgDir + imgName, cv2.IMREAD_COLOR)

    final = cv2.putText(img, text='Rule ' + str(i+1), org=(149,168),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)

    
    cv2.imwrite("/var/www/html/pod" + str(i+1) + "/dnas/" + imgName ,img)

#######################################################################################

    imgDir = "/var/www/html/pod" + str(i+1) + "/cwa/"
    imgName = "cwa-04.png"
    img = cv2.imread(imgDir + imgName, cv2.IMREAD_COLOR)

    final = cv2.putText(img, text='CWA-' + str(i+1), org=(306,289),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)

    final = cv2.putText(img, text='CWA-' + str(i+1), org=(306,332),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)    
    #cv2.imwrite(imgName,img)
    




    imgDir = "/var/www/html/pod" + str(i+1) + "/cwa/"
    imgName = "cwa-10.png"
    img = cv2.imread(imgDir + imgName, cv2.IMREAD_COLOR)

    final = cv2.putText(img, text='CWA-' + str(i+1), org=(574,584),
                fontFace= cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.8, color=(0,0,0),
                thickness=3, lineType=cv2.LINE_AA)
  
    cv2.imwrite("/var/www/html/pod" + str(i+1) + "/cwa/" + imgName ,img)













    
#cv2.imshow('Example',img)




    
